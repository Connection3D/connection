package ConnectFour;

//Represents one of 42 squares in a classic 7X6 grid
public class Square
{
	public SquareContent content;

	public Square()
	{
		content = SquareContent.Blank;
	}
}