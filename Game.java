package ConnectFour;

//Represents a game of classic Connect Four
public class Game
{
	public Grid grid;
	public Player yellowPlayer;
	public Player redPlayer;
	public Player currentPlayer;

	// Constructor
	public Game()
	{
		grid = new Grid();
		redPlayer = new Player(Color.Red);
		yellowPlayer = new Player(Color.Yellow);
		currentPlayer = redPlayer; // Red starts first in this simple
									// implementation. Both players can start in
									// classic game.
	}

	public Game(String yellowPlayerName, String redPlayerName)
	{
		this();
		yellowPlayer.name = yellowPlayerName;
		redPlayer.name = redPlayerName;
	}

	public Outcome getOutcome()
	{
		return grid.getOutcome();
	}

	public String printOutcome()
	{
		Outcome o = grid.getOutcome();
		if (o == Outcome.RedWon)
			return redPlayer.name + " playing red won.";
		else if (o == Outcome.YellowWon)
			return yellowPlayer.name + " playing yellow won.";
		else if (o == Outcome.Draw)
			return "Game ended in a draw.";
		else
			return "Game is ongoing.";
	}

	public Disk newDisk()
	{
		return currentPlayer.newDisk();
	}

	public void switchPlayer()
	{
		if (currentPlayer == redPlayer)
			currentPlayer = yellowPlayer;
		else
			currentPlayer = redPlayer;
	}

	public void play(Drop drop)
	{
		grid.add(drop);
	}

	public String printGrid()
	{
		return grid.print();
	}
}