package ConnectFour;

//import junit.framework.TestCase;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DiskTests
// extends TestCase
{

	@Test
	public void diskCreation()
	{
		// Arrange
		Disk testDisk;

		// Act
		testDisk = new Disk(Color.Red);

		// Assert
		assertTrue(testDisk instanceof Disk);
		// assertThat(testDisk, instanceOf(Disk.class));
		assertTrue(testDisk.color == Color.Red);
	}

	@Test(expected = NullPointerException.class)
	public void diskDeclaredNotCreated()
	{
		// Arrange
		Disk testDisk = null;

		// Act
		@SuppressWarnings("null")
		Color testColor = testDisk.color;

		// Assert
		assertTrue(testDisk == null);
	}

	@Before
	public void Setup()
	{
		// Setup fixtures here. Fixtures can be fields that are shared amongst
		// different tests.
	}

	@After
	public void Teardown()
	{
		// Teardown fixtures here.
	}
}