package ConnectFour;

//Represents a drop (turn?). A game sequence can be represented by a list of drops
public class Drop
{
	public Disk disk;
	public int column;

	public Drop(Disk disk, int column)
	{
		this.disk = disk;
		this.column = column;
	}
}