package ConnectFour;

//Possible game outcome
public enum Outcome
{
	Ongoing, // Game is not done yet
	YellowWon, // Game ended and Yellow player won
	RedWon, // Game ended and Red player has won
	Draw, // Game ended in a draw
}