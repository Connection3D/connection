package ConnectFour;

import java.util.Scanner;

/**
 * Auto Generated Java Class.
 */
public class Program
{

	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);

		System.out.println("ConnectFour 0.1");
		System.out.print("Enter Red player name: ");
		String redName = scan.nextLine();
		System.out.println();

		System.out.print("Enter Yellow player name: ");
		String yellowName = scan.nextLine();
		System.out.println();

		Game game = new Game(yellowName, redName);
		int inputColumn;
		Drop attemptedDrop;
		while (game.getOutcome() == Outcome.Ongoing)
		{
			System.out.println(game.printGrid());
			System.out.print(game.currentPlayer.toString()
					+ ", play a drop in column (0-6)");
			inputColumn = scan.nextInt();
			scan.nextLine(); // Consume leftover newline

			// Should check inputColumn < max columns
			if (inputColumn < 0 || inputColumn > 6)
			{
				System.out.println("Drop played in invalid column, try again.");
				continue;
			}
			attemptedDrop = new Drop(game.newDisk(), inputColumn);
			if (game.grid.isValid(attemptedDrop))
			{
				game.play(attemptedDrop);
				game.switchPlayer();
			} else
				System.out.println("Invalid drop, try again.");
		}
		System.out.println(game.printGrid());
		System.out.println(game.printOutcome());

		scan.close();
	}
}
