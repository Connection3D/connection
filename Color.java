package ConnectFour;

//Represents colors used in classic connect four.
public enum Color
{
	Red, Yellow
}