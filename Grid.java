package ConnectFour;

import java.util.ArrayList;
import java.util.List;

//Represents a vertically suspended Connect Four grid
public class Grid
{
	static int classicNumberOfColumns = 7;
	static int classicNumberOfRows = 6;
	public int numberOfColumns = 7;
	public int numberOfRows = 6;
	public Square[][] squares;
	public List<Drop> drops; // History of drops

	public Grid()
	{
		this(classicNumberOfRows, classicNumberOfColumns);
	}

	public Grid(int numOfRows, int numOfColumns)
	{
		numberOfColumns = numOfColumns;
		numberOfRows = numOfRows;

		squares = new Square[numberOfRows][numberOfColumns];

		for (int col = 0; col < numberOfColumns; col++)
		{
			for (int row = 0; row < numberOfRows; row++)
			{
				squares[row][col] = new Square();
			}
		}
		drops = new ArrayList<Drop>();
	}

	public void Reset()
	{
		for (int col = 0; col < numberOfColumns; col++)
		{
			for (int row = 0; row < numberOfRows; row++)
			{
				squares[row][col].content = SquareContent.Blank;
			}
		}
		drops = new ArrayList<Drop>(); // Reset history of drops
	}

	// Drop is valid if the square for the last (top) row in the drop column is
	// blank.
	public Boolean isValid(Drop drop)
	{
		return (squares[5][drop.column].content == SquareContent.Blank);
	}

	// Get all index of columns that can accept a disk
	public List<Integer> getValidColumns()
	{
		List<Integer> validCols = new ArrayList<Integer>();
		for (int col = 0; col < numberOfColumns; col++)
		{
			if (squares[5][col].content == SquareContent.Blank)
			{
				validCols.add(new Integer(col));
			}
		}
		return validCols;
	}

	public Outcome getOutcome()
	{
		// !Hardcoded to check for winners in 6X7 grid
		// Check for winner in horizontal:Check all rows for winners.
		for (int row = 0; row < numberOfRows - 1; row++)
		{
			for (int col = 0; col < 4 - 1; col++)
			{
				SquareContent testContent = squares[row][col].content;
				if (testContent != SquareContent.Blank)
				{
					if ((squares[row][col].content == squares[row][col + 1].content)
							&& (squares[row][col + 1].content == squares[row][col + 2].content)
							&& (squares[row][col + 2].content == squares[row][col + 3].content))
					{
						if (testContent == SquareContent.Red)
						{
							return Outcome.RedWon;
						} else
						{
							return Outcome.YellowWon;
						}
					}
				}
			}
		}

		// Check for winner in vertical:Check all columns for winners.
		for (int col = 0; col < numberOfColumns; col++)
		{
			for (int row = 0; row < 3; row++)
			{
				SquareContent testContent = squares[row][col].content;
				if (testContent != SquareContent.Blank)
				{
					if ((squares[row][col].content == squares[row + 1][col].content)
							&& (squares[row + 1][col].content == squares[row + 2][col].content)
							&& (squares[row + 2][col].content == squares[row + 3][col].content))
					{
						if (testContent == SquareContent.Red)
						{
							return Outcome.RedWon;
						} else
						{
							return Outcome.YellowWon;
						}
					}
				}
			}
		}

		// Check for winner in diagonal:LA to NY diagonal
		for (int col = 0; col < 3; col++)
		{
			for (int row = 0; row < 3; row++)
			{
				SquareContent testContent = squares[row][col].content;
				if (testContent != SquareContent.Blank)
				{
					if ((squares[row][col].content == squares[row + 1][col + 1].content)
							&& (squares[row + 1][col + 1].content == squares[row + 2][col + 2].content)
							&& (squares[row + 2][col + 2].content == squares[row + 3][col + 3].content))
					{
						if (testContent == SquareContent.Red)
						{
							return Outcome.RedWon;
						} else
						{
							return Outcome.YellowWon;
						}
					}
				}
			}
		}

		// Check for winner in diagonal:Miami to Seattle diagonal
		for (int col = 3; col < 7; col++)
		{
			for (int row = 0; row < 3; row++)
			{
				SquareContent testContent = squares[row][col].content;
				if (testContent != SquareContent.Blank)
				{
					if ((squares[row][col].content == squares[row + 1][col - 1].content)
							&& (squares[row + 1][col - 1].content == squares[row + 2][col - 2].content)
							&& (squares[row + 2][col - 2].content == squares[row + 3][col - 3].content))
					{
						if (testContent == SquareContent.Red)
						{
							return Outcome.RedWon;
						} else
						{
							return Outcome.YellowWon;
						}
					}
				}
			}
		}

		// Check if game is ongoing. Game is not finished if there is a single
		// blank square.
		// TODO: Shortcut by checking top row for blank squares.
		for (int row = 0; row < 6; row++)
		{
			for (int col = 0; col < 7; col++)
			{
				if (squares[row][col].content == SquareContent.Blank)
				{
					return Outcome.Ongoing;
				}
			}
		}

		// At this point, there are no winners AND all squares have been filled
		// therefore game is a draw
		return Outcome.Draw;
	}

	public void add(Drop drop)
	{
		// Should check if it is legal to add drop - throwing exception if not
		// legal
		drops.add(drop);
		for (int row = 0; row < numberOfRows; row++)
		{
			if (squares[row][drop.column].content == SquareContent.Blank)
			{
				if (drop.disk.color == Color.Red)
				{
					squares[row][drop.column].content = SquareContent.Red;
					break;
				} else
				{
					squares[row][drop.column].content = SquareContent.Yellow;
					break;
				}
			}
		}
	}

	public List<Drop> GetPotentialWinningDrops(Player player)
	{
		List<Drop> potentialWinners = new ArrayList<Drop>();

		// For each col in grid
		// /Check if drop in that col is valid
		// /Clone the grid.
		// /Do a drop in that grid
		// /Check if player won.
		return potentialWinners;
	}

	public String print()
	{
		String display = "";
		for (int row = numberOfRows - 1; row >= 0; row--)
		{
			for (int col = 0; col < numberOfColumns; col++)
			{
				display = display + "|" + squares[row][col].content.toString();
			}
			// if (row >= 0)
			display = display + "|\n";
		}
		return display;
	}

}