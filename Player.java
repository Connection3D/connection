package ConnectFour;

//Represents a player ~ start with human player. Later, factor out behaviour so that 
//AI player can be represented. E.g. Minimax, MonteCarlo Player, etc.
public class Player
{
	public String name;
	public Color color;

	public Player(Color color)
	{
		this.color = color;
	}

	public String toString()
	{
		if (color == Color.Red)
			return "Red player";
		else
			return "Yellow player";
	}

	public Disk newDisk()
	{
		return new Disk(color);
	}
}