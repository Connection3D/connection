package ConnectFour;

//Represent possible content of a square
public enum SquareContent
{
	Blank
	{
		public String toString()
		{
			return "B";
		}
	},
	Red
	{
		public String toString()
		{
			return "R";
		}
	},
	Yellow
	{
		public String toString()
		{
			return "Y";
		}
	};
}