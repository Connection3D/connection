package ConnectFour;

//Represents a disk inserted in the game grid.
public class Disk
{
	public Color color;

	public Disk(Color color)
	{
		this.color = color;
	}
}